import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Touchable from 'react-native-platform-touchable';

import Background from '../../components/Background';
import Footer from '../../components/Footer';

export default function Authors() {
  return (
    <Background>
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.title}>Credits</Text>
        </View>

        <View style={styles.content}>
          <Text style={styles.subTitle}>Wallpaper</Text>
          <Text style={styles.description}>
            <Text style={styles.textBold}>needpix.com: </Text>
            Image background login
          </Text>

          <Text style={styles.subTitle}>Images</Text>
          <Text style={styles.description}>
            <Text style={styles.textBold}>br.freepik.com: </Text>
            Negócio vetor criado por dooder -
            br.freepik.com/fotos-vetores-gratis/negocio
          </Text>
        </View>
        <Footer />
      </View>
    </Background>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    marginHorizontal: 10,
  },
  header: {
    backgroundColor: '#fff',
    padding: 10,
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerTitle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: '#222',
    fontWeight: 'bold',
    fontSize: 26,
  },
  subTitle: {
    color: '#222',
    fontSize: 14,
    marginTop: 20,

    width: '100%',
    padding: 5,
    textTransform: 'uppercase',
    borderWidth: 1,
    borderBottomColor: '#d32f2f',
    textAlign: 'center',
  },
  titleMenu: {
    color: '#fff',
    fontSize: 22,
    elevation: 5,
  },
  titleMenuDark: {
    color: '#222',
    fontSize: 22,
    elevation: 5,
  },
  description: {
    color: '#222',
    fontSize: 14,
    marginVertical: 10,
  },
  text: {
    color: '#fff',
    fontSize: 14,
  },
  textBold: {
    color: '#222',
    fontSize: 14,
    fontWeight: 'bold',
  },
});
