import React from 'react';
import {ImageBackground, Text, StyleSheet, View} from 'react-native';
import Touchable from 'react-native-platform-touchable';

import commonStyles from '../../commonStyles';

import bgLogin from '../../../assets/imgs/login.jpg';

export default function Auth({navigation}) {
  return (
    <ImageBackground
      source={bgLogin}
      style={styles.container}
      resizeMode="cover">
      <Text style={styles.title}>My Daily Tasks</Text>

      <View style={styles.content}>
        <View style={styles.containerPhrase}>
          <Text style={styles.phrase}>Se organize, note e anote</Text>
        </View>
        <Touchable onPress={navigation.navigate('Home')} style={styles.button}>
          <Text style={styles.buttonText}>Me Organizar</Text>
        </Touchable>
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10,
  },
  imageBg: {
    width: '100%',
  },
  title: {
    fontFamily: commonStyles.fontFamily,
    color: commonStyles.colors.secondary,
    fontSize: 50,
    marginVertical: 20,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 3,
  },
  containerPhrase: {
    marginBottom: 50,
    backgroundColor: 'rgba(255, 0, 0, 0.2)',
    borderColor: '#fff',
    borderWidth: 1,
    borderRadius: 6,
    padding: 16,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 3,
  },
  phrase: {
    color: '#fff',
    fontSize: 30,
    textAlign: 'center',
  },
  button: {
    padding: 16,
    backgroundColor: '#ad0908',
    borderColor: '#fff',
    borderWidth: 1,
    borderRadius: 26,
    elevation: 6,
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
  },
});
