import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';

/* import {AdMobBanner} from 'react-native-admob'; */

export default class Footer extends Component {
  render() {
    return (
      <View style={styles.container}>
        {/*  <AdMobBanner
          adSize="banner"
          adUnitID="ca-app-pub-6077715838887527/8390051090"
          testDevices={[AdMobBanner.simulatorId]}
          onAdFailedToLoad={error => console.error(error)}
        /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    color: '#fff',
  },
  text: {
    color: '#fff',
  },
});
