import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';

import Auth from './screens/Auth';
import Authors from './screens/Authors';
import TaskList from './screens/TaskList';

const menuRoutes = {
  Home: {
    name: 'Home',
    screen: TaskList,
    navigationOptions: {
      title: 'Home',
    },
  },
  Credits: {
    name: 'Credits',
    screen: Authors,
    navigationOptions: {
      title: 'Credits',
    },
  },
};

const menuNavigator = createDrawerNavigator(menuRoutes);

const mainRoutes = {
  Auth: {
    name: 'Auth',
    screen: Auth,
  },
  Home: {
    name: 'Home',
    screen: menuNavigator,
  },
  Credits: {
    name: 'Credits',
    screen: Authors,
    navigationOptions: {
      title: 'Credits',
    },
  },
};

const mainNavigator = createSwitchNavigator(mainRoutes, {
  initialRouteName: 'Auth',
});

export default createAppContainer(mainNavigator);
